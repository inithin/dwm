/* See LICENSE file for copyright and license details. */
/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 4;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh            = 25;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "Fantasque Sans Mono:size=12", "JoyPixels:pixelsize=12" };
static const char dmenufont[]       = "Fantasque Sans Mono:size=12";
static const char col_gray[]       = "#2E3440";
static const char col_blue[]       = "#88C0D0";
static const char col_red[]       = "#BF616A";
static const char col_yellow[]       = "#EBCB8B";
static const char col_white[]        = "#D8DEE9";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_white, col_gray, col_yellow },
	[SchemeSel]  = { col_gray, col_blue,  col_red },
};

/* tagging */
static const char *tags[] = { "DEV", "WEB", "MES", "MUS", "GAM", "DOC", "OTH", };

static const Rule rules[] = {
        /* xprop(1):
         *      WM_CLASS(STRING) = instance, class
         *      WM_NAME(STRING) = title
         */
       /* class     	   instance       title       tags mask  isfloating  isterminal  noswallow  monitor */
       { "Brave-browser",  NULL,          NULL,       1 << 1,        0,            0,          -1,        -1 },
       { "discord",	   NULL,	  NULL,	      1 << 2,        0,            0,	        1,	  -1 },
       { "Signal",	   NULL,	  NULL,	      1 << 2,        0,            0,           0,        -1 },
       {"Discord Updater", NULL,          NULL,	           0,	     0,		   0,	        1,        -1 },
       { "st-256color",    NULL,          NULL,            0,        0,            1,           0,        -1 },
       {"Pavucontrol",     NULL,	  NULL,	      1 << 6,        1,            0,           1,        -1 },
       {"Transmission",	   NULL,	  NULL,	      1 << 6,        1,            0,           0,        -1 },
       {"Zathura",	   NULL,	  NULL,	      1 << 5,        0,            0,           0,        -1 },
       { "Minecraft Launcher",NULL,	  NULL,		   0,        0,            0,           1,        -1 },
       { NULL,      	   NULL,     "Event Tester",       0,        0,            0,           1,        -1 }, /* xev */
};


/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]=",      tile },    /* first entry is default */
	{ "[F]=",      NULL },    /* no layout function means floating behavior */
        { "[C]=",      centeredmaster },

};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *browsercmd[] = {"brave", NULL};
static const char *discordcmd[] = {"discord", NULL };
static const char *filemgr[] = {"thunar", NULL};
static const char *signalcmd[] = {"signal-desktop", NULL,};


static Key keys[] = {
	/* modifier                     key        			function        argument */
	{ MODKEY,                       XK_d,      			spawn,          {.v = dmenucmd } },
	{ MODKEY,	                XK_Return, 			spawn,          {.v = termcmd } },
	{ MODKEY,			XK_w,	   			spawn,	   	{.v = browsercmd} },
	{ MODKEY|ShiftMask,		XK_d,	   			spawn,	   	{.v = discordcmd} },
	{ MODKEY,			XK_f,				spawn,		{.v = filemgr} },
	{ MODKEY|ShiftMask,		XK_s,				spawn,		{.v = signalcmd} },
	{ MODKEY,			XK_y,				spawn,		SHCMD("st -e ytop") },
	{ MODKEY,                       XK_v,                           spawn,          SHCMD("st -e nvim") },
	{ MODKEY,                       XK_m,                           spawn,          SHCMD("st -e ncmpcpp") },
	{ MODKEY,			XK_x,				spawn,		SHCMD("bash /home/nithin/.local/bin/dmlogout") },
	{ 0, 				XF86XK_AudioMute, 		spawn,		SHCMD("  amixer -q set Master toggle && pkill -RTMIN+3 dwmblocks ") },
	{ 0, 				XF86XK_AudioRaiseVolume,	spawn,	        SHCMD(" amixer -q set Master 5%+ unmute && pkill -RTMIN+3 dwmblocks ") },	
	{ 0, 				XF86XK_AudioLowerVolume,	spawn,	        SHCMD(" amixer -q set Master 5%- unmute && pkill -RTMIN+3 dwmblocks ")	},
	{ 0, 				XF86XK_MonBrightnessUp,		spawn,		SHCMD(" xbacklight -inc 5 && pkill -RTMIN+5 dwmblocks ") },
	{ 0, 				XF86XK_MonBrightnessDown,	spawn,		SHCMD(" xbacklight -dec 5 && pkill -RTMIN+5 dwmblocks ") },
	{ 0, 				XF86XK_TouchpadToggle,		spawn,		SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
	{ 0,				XF86XK_ScreenSaver,		spawn,		SHCMD("slock") },
	{ 0,				XK_Print,			spawn,		SHCMD("maim ~/Pictures/$(date '+%y%m%d-%H%M-%S').png && notify-send 'Screenshot taken' 'saved to ~/Pictures'") },
	{ ShiftMask,			XK_Print,			spawn,		SHCMD("maim -s ~/Pictures/$(date '+%y%m%d-%H%M-%S').png && notify-send 'Screenshot taken' 'saved to ~/Pictures'") },
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_s, 	   zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,	                XK_q,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,             XK_m,      togglefullscr,  {0} }, 
        { MODKEY|ShiftMask,             XK_c,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(			XK_7,			   6)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

